<#macro elOut el >${'${'}${el}${'}'}</#macro>
<%@include file="/taglib.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>{todo}</title>
		<%@include file="/WEB-INF/views/common/commonCss.jsp"%>
	</head>

	<body>
		<div class="admin-main">
			<!-- 查询条件 -->
			<form  class="layui-elem-quote" action="${table.javaProperty}!main.do" id="mainForm" method="post">
				<div class="layui-fr">
					<a href="javascript:;" class="layui-btn layui-btn-small" id="search">
						<i class="layui-icon">&#xe615;</i> 查询
					</a>
				<#if hasEdit?? && hasEdit == 'true'>
					<#if hasAuth?? && hasAuth == 'true'>
					<dyl:hasPermission kind="ADD" menuId="<@elOut el="menuId"/>">
					</#if>
					<a href="javascript:;" class="layui-btn layui-btn-small" id="add">
						<i class="layui-icon">&#xe608;</i> 添加
					</a>
					<#if hasAuth?? && hasAuth == 'true'>
					</dyl:hasPermission>
					</#if>
				</#if>
				<#if hasDelete?? && hasDelete == 'true'>
					<#if hasAuth?? && hasAuth == 'true'>
					<dyl:hasPermission kind="DELETE" menuId="<@elOut el="menuId"/>">
					</#if>
					<a href="javascript:;" class="layui-btn layui-btn-small" id="delete">
						<i class="layui-icon">&#xe640;</i> 删除
					</a>
					<#if hasAuth?? && hasAuth == 'true'>
					</dyl:hasPermission>
					</#if>
				</#if>
				</div>
			<#list table.baseColumns as c>
				<#if c.selectSql??>
    			<#else>
    			<div class="layui-input-inline">
			        <label class="layui-form-label">${c.remarks!c.javaProperty}:</label>
			        <div class="layui-input-inline">
			       		  <input type="text" name="${c.javaProperty}"  value="<@elOut el="${table.javaProperty}.${c.javaProperty}"/>" placeholder="请输入${c.remarks!c.javaProperty}" class="layui-input">
			        </div>
			    </div>
    			</#if>
			</#list>
				
			</form>
			<!-- 主table -->
			<fieldset class="layui-elem-field" >
				<legend>数据列表</legend>
				<div class="layui-field-box layui-form">
					<table class="layui-table admin-table">
						<thead>
							<tr>
								<th class="table-check"><input type="checkbox" name="" lay-skin="primary" lay-filter="allChoose"></th>
							<#list table.baseColumns as c>
								<th>${c.remarks!c.javaProperty}</th>
							</#list>
								<th>操作</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="<@elOut el="${table.javaProperty}List"/>" var="o">
								<tr>
							        <td><input type="checkbox" lay-skin="primary" data-opt="check" data-id="<@elOut el="o.${table.primaryKey.javaProperty}"/>"></td>
							    <#list table.baseColumns as c>
								  <#if c.javaType !='Date'>
									<td><@elOut el="o.${c.javaProperty}"/></td>
								  <#else>
									<td><fmt:formatDate value="<@elOut el="o.${c.javaProperty}"/>" pattern="yyyy-MM-dd HH:mm" /></td>
								  </#if>								
								</#list>
								<#if hasEdit?? && hasEdit == 'true'>
							        <td>
							        	<#if hasAuth?? && hasAuth == 'true'>
										<dyl:hasPermission kind="UPDATE" menuId="<@elOut el="menuId"/>">
										</#if>
										<a href="javascript:;" class="layui-btn layui-btn-mini" data-id="<@elOut el="o.${table.primaryKey.javaProperty}"/>" data-opt="edit">
											<i class="layui-icon">&#xe642;</i> 修改
										</a>
										<#if hasAuth?? && hasAuth == 'true'>
										</dyl:hasPermission>
										</#if>
									</td>
								</#if>	
							    </tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</fieldset>
			<!-- 分页div -->
			<div class="admin-table-page">
				<div id="page" class="page">
				</div>
			</div>
		</div>
		<!-- 通用js -->
		<%@include file="/WEB-INF/views/common/commonJs.jsp"%>
		<!-- 分页 -->
		<%@include file="/WEB-INF/views/common/page.jsp"%>
		<script>
			//查询方法
			$('#search').click(function(){
				//将分页的 的数据带过来
				$('#mainForm').append($('#pageForm').html());
				showLoading();//显示等待框
				$('#mainForm').submit();
			});
			<#if hasEdit?? && hasEdit == 'true'>
			//添加方法
			$('#add').click(function(){
				var para={
					url:"${table.javaProperty}!${table.javaProperty}Form.do",
					title:"添加",
					btnOK:"保存"
				};
				addOrUpdate(para);//通用新增修改方法
			});
			//修改方法
			$('[data-opt=edit]').click(function(){
				var para={
					url:"${table.javaProperty}!${table.javaProperty}Form.do",
					para:"id="+$(this).attr("data-id"),
					title:"修改",
					btnOK:"修改"
				};
				addOrUpdate(para);//通用新增修改方法
			});
			</#if>	
			<#if hasDelete?? && hasDelete == 'true'>
			//删除方法
			$('#delete').click(function(){
				var dataIds = getdataIds("check");
				if(dataIds){
					layer.confirm('确认删除所选择的吗?', function(index){
						getJsonDataByPost("${table.javaProperty}!delete.do","dataIds="+dataIds,function(data){
							if(data.result){
								layer.alert("删除成功!",function(){
									$('#search').click();//查询
								}); 
							}else{
								l.alert(data.msg);//错误消息弹出
							}
							layer.close(index);//关闭删除确认提示框
						},true); 
					});
				}
			});
		</#if>	
		</script>
	</body>
</html>